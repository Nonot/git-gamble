[package]
name = "git-gamble"
version = "2.9.0"
description = "blend TDD + TCR to make sure to develop the right thing 😌, baby step by baby step 👶🦶"
authors = ["pinage404 <pinage404+git-gamble@gmail.com>"]
edition = "2021"
homepage = "https://git-gamble.is-cool.dev"
repository = "https://gitlab.com/pinage404/git-gamble"
keywords = ["TDD", "TCR", "test", "unit-testing", "git"]
categories = [
	"command-line-utilities",
	"development-tools::testing",
	"development-tools",
]
license = "ISC"
readme = "README.md"
include = ["src/**/*", "LICENSE", "README.md", "CHANGELOG.md"]
default-run = "git-gamble"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
clap = { version = "4", features = ["derive", "env", "wrap_help"] }
clap_complete = "4"
human-panic = "2"
log = "0.4"
pretty_env_logger = { version = "0.5", optional = true }
shlex = "1"
yansi = "1"

[dev-dependencies]
assert_cmd = "2"
env_logger = "0.11"
predicates = "3"
pretty_env_logger = "0.5"
speculoos = "0.11"
tempfile = "3"
test-case = "3"
test-log = "0.2"

[features]
with_log = ["pretty_env_logger"]

[profile.release]
# https://github.com/johnthagen/min-sized-rust
# https://arusahni.net/blog/2020/03/optimizing-rust-binary-size.html
opt-level = "z"
lto = "fat"
codegen-units = 1
strip = "symbols"
panic = "abort"

[badges]
gitlab = { repository = "pinage404/git-gamble", branch = "main" }

[package.metadata.release]
# do not manual publish on crates.io with cargo-release, CI should do it
publish = false
allow-branch = ["main"]
tag-prefix = "version"
tag-name = "{{prefix}}/{{version}}"
pre-release-replacements = [
	{ file = "CHANGELOG.md", search = "Unreleased", replace = "{{version}}" },
	{ file = "CHANGELOG.md", search = "\\.\\.\\.HEAD", replace = "...{{tag_name}}", exactly = 1 },
	{ file = "CHANGELOG.md", search = "ReleaseDate", replace = "{{date}}" },
	{ file = "CHANGELOG.md", search = "<!-- next-version -->", replace = "<!-- next-version -->\n## [Unreleased] - ReleaseDate\n\n\n\n[Unreleased]: https://gitlab.com/pinage404/git-gamble/-/compare/{{tag_name}}...HEAD\n", exactly = 1 },
	{ file = "README.md", search = "git-gamble/\\d+\\.\\d+\\.\\d+", replace = "git-gamble/{{version}}" },
	{ file = "docs/src/contributing/development/README.md", search = "git-gamble/\\d+\\.\\d+\\.\\d+", replace = "git-gamble/{{version}}" },
	{ file = "docs/src/install/methods/debian.md", search = "git-gamble_\\d+\\.\\d+\\.\\d+_amd64\\.deb", replace = "git-gamble_{{version}}_amd64.deb" },
	{ file = "docs/src/install/methods/debian.md", search = "git-gamble_\\d+\\.\\d+\\.\\d+-1_amd64\\.deb", replace = "git-gamble_{{version}}-1_amd64.deb" },
	{ file = "docs/src/install/methods/chocolatey.md", search = "git-gamble\\.portable\\.\\d+\\.\\d+\\.\\d+\\.nupkg", replace = "git-gamble.portable.{{version}}.nupkg" },
	{ file = "docs/src/install/methods/chocolatey.md", search = "--version=\\d+\\.\\d+\\.\\d+", replace = "--version={{version}}" },
	{ file = "docs/src/install/methods/binary.md", search = "version%2F\\d+\\.\\d+\\.\\d+", replace = "version%2F{{version}}" },
	{ file = "docs/src/install/_check_the_installation.md", search = "git-gamble \\d+\\.\\d+\\.\\d+", replace = "git-gamble {{version}}" },
	{ file = "packaging/Chocolatey/git-gamble.nuspec", search = "<version>\\d+\\.\\d+\\.\\d+</version>", replace = "<version>{{version}}</version>" },
	{ file = "docs/src/usage/demo/Dockerfile", search = "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-debian/\\d+\\.\\d+\\.\\d+/git-gamble_\\d+\\.\\d+\\.\\d+_x86_64.deb", replace = "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-debian/{{version}}/git-gamble_{{version}}_x86_64.deb" },
]
pre-release-hook = "./script/pre_release_hook.sh"

[package.metadata.binstall]
pkg-fmt = "bin"

[package.metadata.binstall.overrides.x86_64-unknown-linux-gnu]
pkg-url = "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/{ name }-linux/{ version }/{ bin }_v{ version }_x86_64_linux"

[package.metadata.deb]
license-file = ["LICENSE"]
section = "devel"
assets = [
	[
		"target/release/git-gamble",
		"usr/bin/",
		"755",
	],
	[
		"LICENSE",
		"usr/share/doc/git-gamble/",
		"644",
	],
	[
		"CHANGELOG.md",
		"usr/share/doc/git-gamble/CHANGELOG",
		"644",
	],
	[
		"docs/src/usage/command_line_interface.md",
		"usr/share/doc/git-gamble/README",
		"644",
	],
	[
		"target/release/shell_completions/git-gamble.bash",
		"usr/share/bash-completion/completions/git-gamble",
		"644",
	],
	[
		"target/release/shell_completions/git-gamble.fish",
		"usr/share/fish/vendor_completions.d/git-gamble.fish",
		"644",
	],
	[
		"target/release/shell_completions/_git-gamble",
		"usr/share/zsh/vendor-completions/",
		"644",
	],
]
