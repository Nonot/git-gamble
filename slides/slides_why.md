---
favicon: '/logo.svg'
info: >
  git-gamble is a tool that
  blends TDD + TCR to make sure to develop the right thing 😌,
  baby step by baby step 👶🦶
addons:
  - slidev-component-zoom
defaults:
  transition: view-transition
selectable: true
canvasWidth: 800
lineNumbers: true
---

# Why ?

---
layout: center
---

<nav><Toc /></nav>

---
layout: section
---

## 🎮

<!--
Have you ever experienced
-->

---
layout: platform-game
characterPositionX: 1
characterPositionY: 3
caption: "I play a game"
---

---
layout: platform-game
characterPositionX: 2
characterPositionY: 3
caption: "I play a game"
---

---
layout: platform-game
characterPositionX: 3
characterPositionY: 3
caption: "I play a game"
---

<!--
I play a game
-->

---
layout: platform-game
characterPositionX: 4
characterPositionY: 3
caption: "I play a game"
---

<!--
Then i missed something
-->

---
layout: platform-game
characterPositionX: 5
characterPositionY: 4
characterClass: falling
caption: "Then i missed something"
---

---
layout: platform-game
characterPositionX: 5
characterPositionY: 5
characterClass: dead
caption: "Then i missed something"
---

---
layout: statement
---

Game Over ☠️

<!--
Oops !

I pressed the wrong key to jump
-->

---
src: ./pages/restart.md
---

---
layout: platform-game
characterPositionX: 1
characterPositionY: 3
caption: "This time, i won't let it fool me"
---

---
layout: platform-game
characterPositionX: 2
characterPositionY: 3
caption: "This time, i won't let it fool me"
---

---
layout: platform-game
characterPositionX: 3
characterPositionY: 3
caption: "This time, i won't let it fool me"
---

---
layout: platform-game
characterPositionX: 4
characterPositionY: 3
caption: "I will make a quicksave before taking that risky step"
---

<QuickSave v-click />

<!--
This time, i won't let it fool me :

I will make a quicksave before taking that risky step
-->

---
layout: platform-game
characterPositionX: 5
characterPositionY: 4
characterClass: falling
caption: "Oh no, i did it again"
---

---
layout: platform-game
characterPositionX: 5
characterPositionY: 5
characterClass: dead
caption: "Oh no, i did it again"
---

<Loader v-click>💾</Loader>

<!--
Oh no, i did it again

But don't worry, i can load quickly
-->

---
layout: platform-game
characterPositionX: 4
characterPositionY: 3
caption: "I can retry but not from the beginning"
---

<Loader v-click.hide>💾</Loader>

<!--
I can retry but not from the beginning
-->

---
layout: platform-game
characterPositionX: 5
characterPositionY: 2
caption: "Yeah!"
---

---
layout: platform-game
characterPositionX: 6
characterPositionY: 3
button: show
caption: "I did it! Quicksave so that i won't have to restart it again"
---

<QuickSave v-click />

<!--
Yeah ! I did it

I will make a quicksave so that i won't have to restart it again

Weird click ! 🤨
-->

---
layout: platform-game
characterPositionX: 7
characterPositionY: 3
characterClass: dead
spike: show
caption: "I can reload quickly"
---

<Loader v-click>💾</Loader>

<!--
Oh ! That spikes were triggered by the weird click

But i don't care, i just made a quicksave before

I can reload quickly
-->

---
layout: platform-game
characterPositionX: 6
characterPositionY: 3
button: show
caption: "I can reload quickly"
---

<Loader v-click.hide>💾</Loader>

---
layout: platform-game
characterPositionX: 7
characterPositionY: 2
spike: show
caption: ""
---

---
layout: platform-game
characterPositionX: 8
characterPositionY: 3
spike: show
caption: "Finally!"
---

---
layout: statement
---

You win! 🎉

<!--
Fortunately, i had quick saves

This modern game is really well designed
-->

---
layout: section
---

## 🧗

---
layout: boulder-climbing
characterPositionY: -3
caption: "Trying to climb a boulder"
---

<!--
Trying to climb a boulder

without falling on the mattress
-->

---
layout: boulder-climbing
characterPositionY: -4
caption: "Trying to climb a boulder"
---

---
layout: boulder-climbing
characterPositionY: -5
caption: "without falling on the mattress"
---

---
layout: boulder-climbing
characterPositionY: -4
characterClass: falling
caption: "I missed the grip"
---

<!--
I missed the grip

and fallen
-->

---
layout: boulder-climbing
characterPositionY: -3
characterClass: dead
caption: "and fallen"
---

---
src: ./pages/restart.md
---

---
layout: boulder-climbing
characterPositionY: -3
caption: "It might be better if"
---

---
layout: boulder-climbing
characterPositionY: -4
caption: "the mattress was raised as i progressed"
---

<!--
It might be better if the mattress was raised as i progressed
-->

---
layout: boulder-climbing
characterPositionY: -5
mattressPositionY: -3
---

<!--
That way, even if i fall

It doesn't hurt and i can restart from the point where i was confident
-->

---
layout: boulder-climbing
characterPositionY: -5
mattressPositionY: -3
characterClass: falling
caption: "Even if i fall"
---

---
layout: boulder-climbing
characterPositionY: -4
mattressPositionY: -3
caption: "It doesn't hurt"
---

---
layout: boulder-climbing
characterPositionY: -5
mattressPositionY: -3
caption: "I can restart from the point where i was confident"
---

---
layout: boulder-climbing
characterPositionY: -6
mattressPositionY: -4
caption: ""
---

---
layout: statement
---

You win! 🏆

---
layout: fact
---

Doing smaller steps 👶🦶 improve confidence 😌

<!--
As seen with the examples
-->

---
src: ./pages/link_to_git-gamble.md
---

---
src: ./pages/link_to_slides_theory.md
---

---
src: ./pages/link_to_slides_demo.md
---

---
src: ./pages/questions.html
---
