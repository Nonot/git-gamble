#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")

display_changelog_of_version() {
    eval "${DIRECTORY_PATH}/../display_changelog_of_version.sh" "${*}"
}

test_fail_when_no_version_given() {
    if display_changelog_of_version >/dev/null; then
        fail "version is required and should fail when not given"
    fi
}

test_display_changelog_of_version() {
    ACTUAL_CONTENT=$(display_changelog_of_version "1.1.0")

    EXPECTED_CONTENT='## [1.1.0] - 2020-05-25

### Added

* custom logo
* shells completions
  * automatically installed with Debian and Homebrew packages
  * only for Bash, Fish, ZSH

[1.1.0]: https://gitlab.com/pinage404/git-gamble/-/compare/version/1.0.0...version/1.1.0'
    assertEquals "contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

. shunit2
