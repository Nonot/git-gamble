#!/usr/bin/env bash

# fail on first error
set -o errexit
set -o nounset
set -o pipefail

# master branch is needed for homebrew
touch force_stash
git stash --keep-index --include-untracked
git switch master
git rebase -
git push --push-option=ci.skip --push-option=integrations.skip_ci
git switch -
git stash pop
rm force_stash
