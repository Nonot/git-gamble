#!/usr/bin/env sh

set -o errexit
set -o nounset

OUT="$1"

mkdir --parents "$OUT"

git-gamble generate-shell-completions bash >"$OUT/git-gamble.bash"
git-gamble generate-shell-completions elvish >"$OUT/git-gamble.elv"
git-gamble generate-shell-completions fish >"$OUT/git-gamble.fish"
git-gamble generate-shell-completions powershell >"$OUT/_git-gamble.ps1"
git-gamble generate-shell-completions zsh >"$OUT/_git-gamble"
