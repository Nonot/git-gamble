{
  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  nixConfig.extra-substituters = [ "https://git-gamble.cachix.org" ];
  nixConfig.extra-trusted-public-keys = [
    "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I="
  ];

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    snowfall-lib = {
      url = "github:snowfallorg/lib";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane = {
      url = "github:ipetkov/crane";
    };

    mdbook = {
      url = "github:pinage404/mdBook";
      flake = false;
    };
  };

  outputs =
    inputs:
    inputs.snowfall-lib.mkFlake {
      inherit inputs;
      src = ./.;

      overlays = [
        inputs.rust-overlay.overlays.default
      ];

      templates = {
        # run with `nix flake init --template gitlab:pinage404/git-gamble`
        default.description = "A basic flake with git-gamble";
      };

      outputs-builder = channels: {
        formatter = channels.nixpkgs.nixfmt-rfc-style;
      };

      alias = {
        packages.default = "git-gamble";
      };

      snowfall = {
        root = ./nix;
      };
    };
}
