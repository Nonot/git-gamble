use std::thread;
use std::time::Duration;

pub(crate) fn delay_to_ensure_that_the_file_system_is_synced() {
	thread::sleep(Duration::from_millis(50));
}
