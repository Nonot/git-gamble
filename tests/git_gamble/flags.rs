use std::io;
use test_log::test;

use crate::fixture::TestRepository;

#[test]
fn should_not_be_able_to_gamble_that_tests_pass_and_tests_fail_in_the_same_time() -> io::Result<()>
{
	let test_repository = TestRepository::init_dirty()?;

	test_repository
		.gamble_with(&["--pass", "--fail", "--", "true"])
		.failure();

	assert!(test_repository.is_dirty());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_no_new_commit());
	Ok(())
}

#[test]
fn should_require_gambling() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	test_repository.gamble_with(&["--", "true"]).failure();

	assert!(test_repository.is_dirty());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_no_new_commit());
	Ok(())
}
