use speculoos::assert_that;
use std::io;

use crate::fixture::TestRepository;

#[test_log::test]
fn squash_commit() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let commit_message = "squash! first commit";

	let command = test_repository.gamble_with(&["--pass", "--squash", "HEAD", "--", "true"]);

	command.success();
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn squash_commit_with_commit_search() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let commit_message = "squash! initial commit";

	let command = test_repository.gamble_with(&["--pass", "--squash", ":/initial", "--", "true"]);

	command.success();
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn squash_commit_accepts_additional_message_parameters() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let commit_message = "squash! first commit";

	let command = test_repository.gamble_with(&[
		"--pass",
		"--squash",
		"HEAD",
		"--message",
		"More information",
		"--",
		"true",
	]);

	command.success();
	assert_that(&test_repository.head_message())
		.is_equal_to(commit_message.to_owned() + "\n\nMore information\n");
	Ok(())
}
