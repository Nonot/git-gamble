pub(crate) mod commit_hooks;

pub(crate) mod commit_message;

pub(crate) mod dry_run;

pub(crate) mod edit_commit_message;

pub(crate) mod fixup_commit;

pub(crate) mod flags;

pub(crate) mod generate_shell_completions;

pub(crate) mod repository_path_can_be_elsewhere;

pub(crate) mod run_hook;

pub(crate) mod squash_commit;

pub(crate) mod test_command_can_be_complex;

pub(crate) mod test_command_is_required;

pub(crate) mod trigger_custom_hooks;

pub(crate) mod when_gambling_that_the_tests_fail;

pub(crate) mod when_gambling_that_the_tests_pass;
