use std::env;
use std::io;
use test_case::test_case;

use crate::fixture::execute_cli_with;
use crate::fixture::TestRepository;

#[test_log::test]
fn should_use_current_working_directory_by_default() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	assert!(env::set_current_dir(test_repository.path()).is_ok());
	execute_cli_with("git-gamble", &["--pass", "--", "true"]).success();

	assert!(test_repository.is_clean());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_new_commit());
	Ok(())
}

#[test_case("--repository-path")]
#[test_case("-C")]
#[test_log::test]
fn should_use_path_when_giving_it_in_option(option: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let path = test_repository.path().to_str().unwrap();
	execute_cli_with("git-gamble", &[option, path, "--pass", "--", "true"]).success();

	assert!(test_repository.is_clean());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_new_commit());
	Ok(())
}
