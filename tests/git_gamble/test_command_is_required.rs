use std::io;
use test_case::test_case;

use crate::fixture::execute_cli;
use crate::fixture::TestRepository;

#[test_log::test]
fn should_abort_without_test_command() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-gamble")
		.current_dir(test_repository.path())
		.env_remove("GAMBLE_TEST_COMMAND")
		.args(["--pass", "--"])
		.assert()
		.failure();

	assert!(test_repository.is_dirty());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_no_new_commit());
	Ok(())
}

#[test_case("true" ; "simple command")]
#[test_case("sh -c \"test 'hello world' = 'hello world'\"" ; "complex command")]
#[test_log::test]
fn should_be_able_to_use_test_command_from_environment_variable_with(
	test_command: &str,
) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-gamble")
		.current_dir(test_repository.path())
		.env("GAMBLE_TEST_COMMAND", test_command)
		.args(["--pass"])
		.assert()
		.success();

	assert!(test_repository.is_clean());
	assert!(test_repository.changes_remain_in_the_working_file());
	assert!(test_repository.has_new_commit());
	Ok(())
}
