use predicates::prelude::*;
use std::io;
use std::process::Command;

use crate::fixture::execute_cli;
use crate::fixture::CommandIgnoreOutputExt;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;

#[test]
fn with_iteration_duration_immediately_hand_over() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "10000")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	Ok(())
}

#[test]
fn without_iteration_duration_it_fails() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.args(["start"])
		.assert()
		.failure();

	Ok(())
}

#[test]
fn without_iteration_duration_display_an_help_message() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.args(["start"])
		.assert();

	command.stderr(
		predicate::str::contains("the following required arguments were not provided:")
			.and(predicate::str::contains("ITERATION_DURATION")),
	);
	Ok(())
}

#[test]
fn can_pass_iteration_duration_as_argument() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.args(["start", "--iteration-duration", "1"])
		.assert()
		.success();

	Ok(())
}

#[test]
fn iteration_duration_is_not_required_when_stopping() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.args(["stop"])
		.assert()
		.success();

	Ok(())
}
