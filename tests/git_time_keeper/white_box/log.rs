use predicates::prelude::*;
use std::io;
use std::process::Command;
use std::thread;
use std::time::Duration;

use crate::fixture::command_ignore_output::CommandIgnoreOutputExt;
use crate::fixture::delay_to_ensure_that_the_file_system_is_synced;
use crate::fixture::execute_cli;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;

#[test_log::test]
fn started_when_starting() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.assert()
		.success();

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains(
		"Started with an iteration duration of 1s",
	));
	Ok(())
}

#[test_log::test]
fn already_started_when_starting_twice() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.assert()
		.success();

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains("Already started, do nothing"));
	Ok(())
}

#[test_log::test]
fn cannot_be_stopped_because_it_is_not_currently_running_when_stopping_without_starting(
) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["stop"])
		.assert()
		.success();

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains(
		"Cannot be stopped because it is not currently running, do nothing",
	));
	Ok(())
}

#[test_log::test]
fn stopped_when_stopping() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["stop"])
		.assert()
		.success();

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains("Stopped"));
	Ok(())
}

#[test_log::test]
fn stepped_back_when_timed_out() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains("Stepped back"));
	Ok(())
}

#[test_log::test]
fn stopped_while_waiting_do_nothing_when_stopped_while_waiting() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let current_dir = format!("{}", &test_repository.path().display());

	let thread_to_stop = thread::spawn(|| {
		delay_to_ensure_that_the_file_system_is_synced();
		Command::new("git-time-keeper")
			.current_dir(current_dir)
			.with_crate_binaries_in_the_path()
			.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
			.args(["stop"])
			.ignore_output()
			.spawn()
			.expect("Can't execute git time-keeper stop in parallele");
	});

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.assert()
		.success();

	thread_to_stop.join().unwrap();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains(
		"Stopped while waiting, do nothing",
	));
	Ok(())
}

#[test_log::test]
fn waiting_when_started() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = execute_cli("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("RUST_LOG", "trace")
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.assert()
		.success();

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	command.stderr(predicate::str::contains("Waiting 1s"));
	Ok(())
}
