# Install using Nix

[![Available on nixpkgs](https://repology.org/badge/vertical-allrepos/git-gamble.svg?exclude_sources=modules)](https://repology.org/project/git-gamble/versions)

[![Packaged for Nixpkgs](https://raw.githubusercontent.com/dch82/Nixpkgs-Badges/main/nixpkgs-badge-dark.svg)](https://search.nixos.org/packages?channel=unstable&show=git-gamble)

[Nix is a package manager](https://nixos.org) available for Linux, Mac OS X and Windows (through WSL2)

## Requirements

1. [Install Nix](https://determinate.systems/nix-installer/)
1. Check the installation with this command

   ```sh
   nix --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   nix (Nix) 2.22.3
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   nix: command not found
   ```

## Install from Nixpkgs

The package is available on [nixpkgs](https://search.nixos.org/packages?channel=unstable&show=git-gamble)

```bash
nix-shell --packages git-gamble
```

{{#include ../_check_the_installation.md}}

## Install from source

Note: the latest version, not yet released, is also available

Use without installing

```sh
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix run gitlab:pinage404/git-gamble -- --help
```

To install it at project level with [`flake.nix`, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/nix/templates/default/)

```sh
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix flake init --template gitlab:pinage404/git-gamble
```

### DirEnv

To automate the setup of the environment it's recommanded to install [DirEnv](https://direnv.net/)

Then run this command

```sh
direnv allow
```

### Cachix

If you want to avoid rebuilding from scratch, you can use [Cachix](https://app.cachix.org/cache/git-gamble#pull)

```sh
cachix use git-gamble
```

### Update

```sh
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix flake update
```
