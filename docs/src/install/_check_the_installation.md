<!-- markdownlint-disable MD041 -->
## Check the installation

Check if all have been well settled

```sh
git gamble --version
```

If it has been **well settled** {{#include ../_logo_mini_front.html}}, it should output this :

```txt
git-gamble 2.9.0
```

Else if it has been **badly settled** {{#include ../_logo_mini_back.html}}, it should output this :

```txt
git : 'gamble' is not a git command. See 'git --help'.
```
