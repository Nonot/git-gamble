<!-- markdownlint-disable-next-line MD041 -->
## Improving installation

Installation is currently not really convenient, so [contributions are welcome](https://git-gamble.is-cool.dev/contributing/index.html)

Feel free to [add package to your favorite package repository](https://git-gamble.is-cool.dev/contributing/add_to_package_repository.html)
