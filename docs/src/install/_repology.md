<!-- markdownlint-disable-next-line MD041 -->
The package is [available on these repositories](https://repology.org/project/git-gamble/versions), and can be installed as usual if you use one of them

[![Packaging status](https://repology.org/badge/vertical-allrepos/git-gamble.svg?exclude_unsupported=1)](https://repology.org/project/git-gamble/versions)
