---
status: "accepted"
date: 2024-11-25
decision-makers: pinage404
---

# Pipeline one source of thruth

## Context and Problem Statement

### Same tools, differents versions

There is no **single source of thruth** between **local development** and **pipeline**

Tools are defined in differents files

| Local development                          | Pipeline                           |
|:-------------------------------------------|:-----------------------------------|
| [`nix/shells/rust/default.nix`]            | [`ci/rust_with_git.Dockerfile`]    |
| [`nix/shells/rust/default.nix`]            | [`ci/rust_for_windows/Dockerfile`] |
| [`nix/shells/rust/default.nix`]            | [`ci/cargo-tarpaulin.Dockerfile`]  |
| [`nix/shells/rust/default.nix`]            | [`ci/shunit2.Dockerfile`]          |
| [`nix/shells/docs/default.nix`]            | [`ci/mdBook.Dockerfile`]           |
| [`nix/shells/advanced_checks/default.nix`] | [`ci/cargo-audit.Dockerfile`]      |
|                                            | [`packaging/debian/Dockerfile`]    |
| [`nix/shells/slides/default.nix`]          | [`.gitlab-ci.yml`]                 |

[`.gitlab-ci.yml`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/.gitlab-ci.yml#L387
[`ci/cargo-audit.Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/cargo-audit.Dockerfile#L13
[`ci/cargo-tarpaulin.Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/cargo-tarpaulin.Dockerfile#L10
[`ci/mdBook.Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/mdBook.Dockerfile#L4
[`ci/rust_for_windows/Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/rust_for_windows/Dockerfile#L17
[`ci/rust_with_git.Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/rust_with_git.Dockerfile#L5
[`ci/shunit2.Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/ci/shunit2.Dockerfile#L7
[`nix/shells/advanced_checks/default.nix`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/nix/shells/advanced_checks/default.nix
[`nix/shells/docs/default.nix`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/nix/shells/docs/default.nix
[`nix/shells/rust/default.nix`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/nix/shells/rust/default.nix
[`nix/shells/slides/default.nix`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/nix/shells/slides/default.nix
[`packaging/debian/Dockerfile`]: https://gitlab.com/pinage404/git-gamble/-/blob/554e7284ab007641795f860a9cc57df79a3b1ece/packaging/debian/Dockerfile#L4

The tools' versions used are not the same depending on the package manager used to install the tools

### Big containers images

Debian based containers images are big

Past attempts have been failures to make working build with Alpine based container, because Alpine use Musl instead of GLibC

#### Example

The image created from [`ci/rust_with_git.Dockerfile`]

```dockerfile
FROM rust:1.80-slim-bookworm

RUN apt-get --quiet=2 update \
    && apt-get --quiet=2 install --no-install-recommends \
    git \
    && apt-get --quiet=2 clean \
    && rm --force --recursive /var/lib/apt/lists/*
```

Using the following commands

```sh
docker build -f ./ci/rust_with_git.Dockerfile ./ci/
dive sha256:443b8185bfb389687697e4193b569feb40a3e0b5a2b023562f8da279516f5d83
```

* Total Image size: 854MB
* Potential wasted space: 4.7 MB
* Image efficiency score: 99 %

### Bad update process

Update is done manually in each `Dockerfile`, in the `FROM` ; which is not convenient

## Decision Drivers

* Open source
* Pricing : must be free or very low fee
* High reproductibility
* Ability to use cache (Nix's cache nor GitLab's cache)
* Lighter
* Faster
* Use standard tools

## Considered Options

* Using external services
  * [NixCI](#nixci)
  * [GitHub Actions](#github-actions)
  * [Garnix](#garnix)
  * [Cachix](#cachix)
  * [Jetify Cache](#jetify-cache)
* Using GitLab only
  * [Export Nix Store to GitLab CI's Cache](#export-nix-store-to-gitlab-cis-cache)
  * [Simple `direnv allow`](#simple-direnv-allow)
  * [Install Nix inside GitLab's job](#install-nix-inside-gitlabs-job)
  * [Build image using Nix](#build-image-using-nix)

## Decision Outcome

Replace [building images with Kaniko in the pipeline](https://gitlab.com/pinage404/git-gamble/-/blob/0922d07bbdac4535cc184a236d7a3872799f58d7/.gitlab-ci.yml#L50-137)
with [building images using Nix](#build-image-using-nix)

Try to [use GitLab's Cache](#export-nix-store-to-gitlab-cis-cache) nor [Cachix](#cachix) using [Cynerd's gitlab-ci-nix example](https://gitlab.com/Cynerd/gitlab-ci-nix) (the tool or the idea did by the tool) to save Nix Store

### Consequences

* **Good**, **one source of truth** for the versions used (which ends up to `flake.lock`)
* **Good**, **update** process is [already handled **automatically**](https://gitlab.com/pinage404/git-gamble/-/blob/0922d07bbdac4535cc184a236d7a3872799f58d7/maskfile.md#update)

  Just execute

  ```sh
  nix flake update
  ```

* **Good**, use **open source** tools
  * Maybe not the Cachix server which will maybe used
* **Good**, **free**
* **Good**, high **reproductibility**
* **Good**, **cache** may be used
* **Bad**, the images are **twice bigger**
  * It probably could be optimized
* **Bad**, expect to be **slower**, because of the image size
  * On self hosted runner, it will use Docker's cache
* **Good**, don't use a niche **tool**

## Pros and Cons of the Options

### NixCI

[NixCI](https://nix-ci.com)

* Source not found
  * Not open source ? ❌
* Only one dev behind it ?
  * What about the maintainability ? ⚠️
* The page talks about GitLab integration
  * There is no call to action for GitLab
    * Don't know how to use it with GitLab 🤷
* Is it SaaS or Self Hosted ?
  * There are machines that will compute stuff
    * Who pays ? Pricing is unclear 💸
  * The page talks about self-hostable
    * Don't found the documentation 🤷
* Need scaring authorization on GitHub : [Act on your behalf](https://docs.github.com/apps/using-github-apps/authorizing-github-apps#about-github-apps-acting-on-your-behalf) 🤨

The page looks more like a roadmap than the documentation of actual functionalities

Doesn't look trustworthy

### GitHub Actions

Need to move to GitHub

* [`cachix/install-nix-action`](https://github.com/cachix/install-nix-action)
  * Seems to run [the official Nix installer](https://github.com/cachix/install-nix-action/blob/08dcb3a5e62fa31e2da3d490afc4176ef55ecd72/install-nix.sh#L96)
* [`DeterminateSystems/nix-installer-action`](https://github.com/DeterminateSystems/nix-installer-action) & [`DeterminateSystems/magic-nix-cache-action`](https://github.com/DeterminateSystems/magic-nix-cache-action)
  * Rely on big TypeScript program that does stuffs 🤷

GitHub is not Open Source ❌

### Garnix

[Garnix](https://garnix.io)

[Pricing have a free tier](https://garnix.io/pricing) (1 500 minutes/month) that may be enougth 💶

* Need scaring authorization on GitHub : [Act on your behalf](https://docs.github.com/apps/using-github-apps/authorizing-github-apps#about-github-apps-acting-on-your-behalf) 🤨
* Need to move [to GitHub](#github-actions) ❌

### Cachix

[Cachix](https://www.cachix.org)

Already [used in the pipeline](https://gitlab.com/pinage404/git-gamble/-/blob/945bff4d41cc6ca25fbf0dfd27e11fd0c745b483/.gitlab-ci.yml#L358) 👍

* [Pricing have a free tier](https://www.cachix.org/pricing) (5Go per project)
  * 2.7Go are already used, it may be not enough 😕
* [Cachix's client is open source](https://github.com/cachix/cachix) but don't found the server ❌
  * Simple Nix instance ? 🤷

### Jetify Cache

[Jetify Cache](https://www.jetify.com/cache)

* [Pricing](https://www.jetify.com/cloud/pricing) seems to start from $5/month + $0.60 Go/month 💰
* Don't found the sources ❌

### Export Nix Store to GitLab CI's Cache

* [Vonfry's gitlab-ci-nix example](https://gitlab.com/Vonfry/gitlab-ci-nix/-/blob/fd5bc0abe2548a87dc511dae73c3b31d1954cb49/.gitlab-ci.yml)
  1. _By GitLab CI itself_, download previous cache, if any ⬇️
  1. [Import derivations to Nix Store from cache](https://gitlab.com/Vonfry/gitlab-ci-nix/-/blob/fd5bc0abe2548a87dc511dae73c3b31d1954cb49/.gitlab-ci.yml#L7), if any ⬅️
  1. _Do stuff_ ⚙️
  1. [Export **all** derivations from Nix Store to cache](https://gitlab.com/Vonfry/gitlab-ci-nix/-/blob/fd5bc0abe2548a87dc511dae73c3b31d1954cb49/.gitlab-ci.yml#L10) ➡️
  1. _By GitLab CI itself_, upload cache ⬆️
* [Cynerd's gitlab-ci-nix example](https://gitlab.com/Cynerd/gitlab-ci-nix)
  1. _By GitLab CI itself_, download previous cache, if any ⬇️
  1. [Set substituters : local cache, Cachix, S3, SSH](https://gitlab.com/Cynerd/gitlab-ci-nix/-/blob/70024ed12bd6b559399b6abf780319ce3608c273/before.sh#L6), if any ⬅️
  1. [Save current derivations](https://gitlab.com/Cynerd/gitlab-ci-nix/-/blob/70024ed12bd6b559399b6abf780319ce3608c273/before.sh#L30)
  1. _Do stuff_ ⚙️
  1. [List new derivations by comparing current derivations with the saved ones](https://gitlab.com/Cynerd/gitlab-ci-nix/-/blob/70024ed12bd6b559399b6abf780319ce3608c273/after.sh#L16)
  1. [Export new derivations from Nix Store to cache](https://gitlab.com/Cynerd/gitlab-ci-nix/-/blob/70024ed12bd6b559399b6abf780319ce3608c273/after.sh#L24) ➡️
  1. [Upload new derivations to Cachix, S3, SSH](https://gitlab.com/Cynerd/gitlab-ci-nix/-/blob/70024ed12bd6b559399b6abf780319ce3608c273/after.sh#L32), if any ⬆️
  1. _By GitLab CI itself_, upload cache ⬆️
* [TECHNOFAB's nix-gitlab-ci template](https://gitlab.com/TECHNOFAB/nix-gitlab-ci)

  Declare pipeline using [Nix](https://gitlab.com/pinage404/dotfiles/-/blob/72e051fb0c14ab2fb1ac23492bec44e5ee496b58/story.md#disliking-nix-language) instead of [YAML](https://noyaml.com/)

  Seems [easy to test locally](https://gitlab.com/TECHNOFAB/nix-gitlab-ci/-/blob/1101989255903a1f20781e007da3b313b9277173/README.md#run-jobs-locally)

  1. [declare the pipeline using Nix](https://gitlab.com/TECHNOFAB/nix-gitlab-ci/-/blob/1101989255903a1f20781e007da3b313b9277173/flake.nix#L42)
  1. [generate the `.gitlab-ci.yaml`](https://gitlab.com/TECHNOFAB/nix-gitlab-ci/-/blob/1101989255903a1f20781e007da3b313b9277173/templates/nix-gitlab-ci.yml#L44)
  1. [trigger a pipeline from the generated `.gitlab-ci.yaml`](https://gitlab.com/TECHNOFAB/nix-gitlab-ci/-/blob/1101989255903a1f20781e007da3b313b9277173/templates/nix-gitlab-ci.yml#L60)

  The generated pipeline works likely Cynerd's version

  Support GitLab's Cache, [Cachix](#cachix), and [Attic](https://github.com/zhaofengli/attic)

As the time pass, the cache will grow, it will slow the pipeline 📈

* Every cache paths will be downloaded and reuploaded at each pipeline, not only the ones that are actually needed for the current job
* How to do clean unecessary paths ? 🚮
  * Should an extra step with `nix-gc` be added ?
  * Without erasing everything periodically ?

| 🆚               | Vonfry's version  | Cynerd's version  | TECHNOFAB's version  |
|:-----------------|:------------------|:------------------|:---------------------|
| Complexity       | Simpler ✅         | More complex 👎   | Even more complex 😵 |
| Cache            | Naive ❌           | Optimized ✅       | Optimized ✅          |
| Reusability      | Just an example ❌ | Made for ✅        | Made for ✅           |
| Reproductibility | Not versionned 👎 | Not versionned 👎 | Have [releases] ✅    |

[releases]: https://gitlab.com/TECHNOFAB/nix-gitlab-ci/-/releases

### Simple `direnv allow`

Use container from `nixos/nix` and run the following command inside

```sh
direnv allow
```

Pro : simple ✅

Const : don't use cache, so download everything from scratch at each jobs ❌

### Install Nix inside GitLab's job

Won't use GitLab's cache ❌

1. Download a base image
1. [Install Nix inside GitLab's job](https://github.com/DeterminateSystems/nix-installer/blob/82a64440cb9d4fc9d1b890e1487ea40497afc5f4/README.md#on-gitlab)
1. Install packages using Nix

Seems to be heavy

### Build image using Nix

* Interesting blog post explaining [how to build image using Nix inside GitLab CI](https://scvalex.net/posts/68/)
* [NixOS official documentation about `dockerTools`](https://nixos.org/manual/nixpkgs/stable/#sec-pkgs-dockerTools) in the too long all in one page heavy manual
  * [Content of the official documentation about `dockerTools`](https://github.com/NixOS/nixpkgs/blob/ea8474c17e52a4aa35f1abb94c4c4ac098c516ba/doc/build-helpers/images/dockertools.section.md) in a lighter page

#### `dockerTools.buildImage`

* [`dockerTools.buildImage`](https://github.com/NixOS/nixpkgs/blob/0de1b65cf2cc9d9988cc076291df45f710f5d22c/doc/build-helpers/images/dockertools.section.md#buildimage-ssec-pkgs-dockertools-buildimage)
  * Produce a container image with one big layer

##### `dockerTools.buildImage` Proof of concept

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile rustupToolchainFile;
in
pkgs.dockerTools.buildImage {
  name = "build-image";
  copyToRoot = [
    rust-toolchain
    pkgs.dockerTools.binSh
  ];
}
```

```sh
nix build '.#build-image'
dive --source docker-archive <(gunzip -c ./result)
```

* Total Image size: 1.9 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

#### `dockerTools.buildLayeredImage`

* [`dockerTools.buildLayeredImage`](https://github.com/NixOS/nixpkgs/blob/0de1b65cf2cc9d9988cc076291df45f710f5d22c/doc/build-helpers/images/dockertools.section.md#buildlayeredimage-ssec-pkgs-dockertools-buildlayeredimage)
  * Produce a container image with several ordered layers
  * Each layers are stored in the Nix Store

##### `dockerTools.buildLayeredImage` Proof of concept

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile rustupToolchainFile;
in
pkgs.dockerTools.buildLayeredImage {
  name = "build-layered-image";
  contents = [
    rust-toolchain
    pkgs.dockerTools.binSh
  ];
}
```

```sh
nix build '.#build-layered-image'
dive --source docker-archive <(gunzip -c ./result)
```

* Total Image size: 1.7 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

##### `dockerTools.buildLayeredImage` Proof of concept limiting max layers

Same when trying to limit max layers

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile rustupToolchainFile;
in
pkgs.dockerTools.buildLayeredImage {
  name = "build-layered-image-limit-layers";
  contents = [
    rust-toolchain
    pkgs.dockerTools.binSh
  ];
  maxLayers = 2;
}
```

```sh
nix build '.#build-layered-image-limit-layers'
dive --source docker-archive <(gunzip -c ./result)
```

* Total Image size: 1.7 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

##### `dockerTools.buildLayeredImage` Proof of concept without components

Same when trying to remove components to remove the rust's source

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rustupToolchainFileContent = lib.trivial.importTOML rustupToolchainFile;
  rustupToolchain = rustupToolchainFileContent.toolchain;

  rustupToolchainCleaned = lib.snowfall.attrs.merge-deep [
    rustupToolchain
    { components = [ ]; }
  ];

  rust-toolchain = pkgs.rust-bin.fromRustupToolchain rustupToolchainCleaned;
in
pkgs.dockerTools.buildLayeredImage {
  name = "build-layered-image-no-rust-src";
  contents = [
    rust-toolchain
    pkgs.dockerTools.binSh
  ];
}
```

```sh
nix build '.#build-layered-image-no-rust-src'
dive --source docker-archive <(gunzip -c ./result)
```

* Total Image size: 1.7 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

#### `dockerTools.streamLayeredImage`

* [`dockerTools.streamLayeredImage`](https://github.com/NixOS/nixpkgs/blob/0de1b65cf2cc9d9988cc076291df45f710f5d22c/doc/build-helpers/images/dockertools.section.md#streamlayeredimage-ssec-pkgs-dockertools-streamlayeredimage)
  * Produce a container image with several ordered layers
  * Layers are **NOT** stored in the Nix Store

##### `dockerTools.streamLayeredImage` Proof of concept

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile rustupToolchainFile;
in
pkgs.dockerTools.streamLayeredImage {
  name = "stream-layered-image";
  contents = [
    rust-toolchain
    pkgs.dockerTools.binSh
  ];
}
```

```sh
nix build '.#stream-layered-image'
./result > stream-layered-image.tar
dive docker-archive://./stream-layered-image.tar
```

* Total Image size: 1.7 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

#### `nix2container`

[`nix2container`](https://github.com/nlewo/nix2container) generate the json that describe the container where the layers are derivation linked to the Nix store

[The article that explain what does it solve](https://lewo.abesis.fr/posts/nix-build-container-image/)

##### `nix2container` Proof of concept

```nix
{
  lib,
  pkgs,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile rustupToolchainFile;
in
pkgs.nix2containerPkgs.nix2container.buildImage {
  name = "nix2container";
  copyToRoot = pkgs.buildEnv {
    name = "root";
    paths = [
      rust-toolchain
      pkgs.dockerTools.binSh
    ];
    pathsToLink = [ "/bin" ];
  };
}
```

```sh
nix run '.#nix2container.copyToDockerDaemon'
dive nix2container:cvlrpzfkvq2y0q99aa8csni1r6znl3bg
```

* Total Image size: 1.7 GB
* Potential wasted space: 0 B
* Image efficiency score: 100 %

#### Comparison

|            | `buildImage`  | `buildLayeredImage` | `streamLayeredImage` | `nix2container` |
|:-----------|:--------------|:--------------------|:---------------------|:----------------|
| Nix Store  | GZipped Image | GZipped Image       | Script + JSON        | JSON            |
| Layers     | 1             | Several             | Several              | 1               |
| Image size | 1.9 GB        | 1.7 GB              | 1.7 GB               | 1.7GB           |

Rust's source code, Linux's headers, locales, documentation and manual pages are included

It could probably be removed with more configuration

## More Information

* [Erica's presentation](https://multi-coop.gitlab.io/slides/multi-facettes/docker-nix/presentation.html) who gaves motivation to deep dive to find a solution
  * [Real world example](https://github.com/agatha-budget/agatha-budget/blob/c1f61715bf0645777aee9282e66e2ff9191388a9/back/deploy/back-docker.nix#L9) to build container image from Nix
