# Deployment

Just run [cargo release](https://crates.io/crates/cargo-release) (in a wide terminal, in order to have `--help` not truncated in [the usage section](../usage/command_line_interface.md))

```sh
cargo release
```
