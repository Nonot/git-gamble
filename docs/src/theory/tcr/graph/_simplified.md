```mermaid
flowchart TB
    gamble{{Gambling on test results}}

    gamble ==>|Pass| commit(Commit)

    gamble -->|Fail| revert(Revert)
```
