_[**TCR** (`test && commit || revert`)][tcr] is cool!_

<v-click>

It **encourages** doing **baby steps**, reducing the waste when we are wrong

</v-click>

[tcr]: https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864
