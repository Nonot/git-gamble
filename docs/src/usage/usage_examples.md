# Usage examples

There is several examples with several languages in [the nix-sandboxes repository](https://gitlab.com/pinage404/nix-sandboxes)

In each language folder, there is a `.envrc` file, which contains the variable `GAMBLE_TEST_COMMAND`, that mainly refers to [`mask` commands](https://github.com/jacobdeichert/mask/) that are described in the sibling file `maskfile.md`
