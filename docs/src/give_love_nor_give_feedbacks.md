# Give Love ❤️ nor Give Feedbacks 🗣️

## Do you like this project ?

<!-- markdownlint-disable-next-line MD039 MD045 -->
- If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/git-gamble?style=social)](https://gitlab.com/pinage404/git-gamble)
<!-- markdownlint-disable-next-line MD045 -->
- If no, please [open an issue](https://gitlab.com/pinage404/git-gamble/-/issues) to give your feedbacks [![open an issue](https://img.shields.io/gitlab/issues/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/issues)
