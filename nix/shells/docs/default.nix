{
  pkgs,
}:

pkgs.mkShellNoCC {
  packages =
    [
      pkgs.mask
    ]
    ++ import ./packages.nix {
      inherit pkgs;
    };
}
