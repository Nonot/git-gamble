{
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-unchanged pkgs;
in
pkgs.mkShellNoCC {
  packages =
    [
      # rust environment
      pkgs.rustup
      rust-toolchain
      pkgs.stdenv.cc

      # needed by git-gamble itself
      pkgs.busybox
      pkgs.gitMinimal

      # needed only to detect what can be improved
      pkgs.cargo-mutants

      # needed only when releasing a new version
      pkgs.cargo-release
    ]
    ++ lib.optionals pkgs.stdenv.isx86_64 [
      # needed only to know the test coverage
      pkgs.cargo-tarpaulin
    ];
}
