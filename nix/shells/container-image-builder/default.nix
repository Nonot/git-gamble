{
  pkgs,
}:

let
  add_skopeo_policy = pkgs.writeShellScriptBin "add_skopeo_policy" ''
    mkdir --parents "/etc/containers"
    cp "${pkgs.skopeo.policy}/default-policy.json" "/etc/containers/policy.json"
  '';
in
pkgs.mkShellNoCC {
  packages = [
    pkgs.skopeo
    add_skopeo_policy
  ];
}
