{
  pkgs,
}:

pkgs.mkShellNoCC {
  packages = import ./packages.nix {
    inherit pkgs;
  };
}
