{
  pkgs,
}:

[
  pkgs.shunit2
  pkgs.coreutils
  pkgs.busybox
  pkgs.gitMinimal
  pkgs.bash
]
