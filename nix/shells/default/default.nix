{
  inputs,
  pkgs,
}:

pkgs.mkShellNoCC {
  inputsFrom = [
    inputs.self.devShells."${pkgs.system}".rust
    inputs.self.devShells."${pkgs.system}".shell
  ];

  packages = [
    # allow to use `git-gamble` to build `git-gamble`
    pkgs.git-gamble

    pkgs.mask
    pkgs.masklint
    pkgs.shellcheck

    # usefull for refactoring session
    pkgs.bacon

    # needed by VSCode extension to format Nix files
    pkgs.nil

    # bash still usable in interactive mode
    pkgs.bashInteractive
  ];
}
