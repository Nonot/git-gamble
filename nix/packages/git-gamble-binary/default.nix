{
  inputs,
  lib,
  pkgs,
}:

let
  root = lib.snowfall.fs.get-file "";

  rust-toolchain = lib.internal.rust-toolchain-minimal pkgs;

  craneLib = (inputs.crane.mkLib pkgs).overrideToolchain rust-toolchain;
in
craneLib.buildPackage {
  pname = "git-gamble-binary";
  src = craneLib.cleanCargoSource root;
  cargoArtifacts = inputs.self.packages."${pkgs.system}".git-gamble-deps;

  doCheck = false;
}
