{
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-with {
    inherit pkgs;
    toolchainOverride = {
      components = [
        "clippy"
        "rustfmt"
      ];
      profile = "minimal";
    };
  };
in
pkgs.dockerTools.buildLayeredImage {
  name = "container-image-rust-check";
  contents = [
    pkgs.dockerTools.binSh
    pkgs.dockerTools.caCertificates
    rust-toolchain
    pkgs.stdenv.cc
    pkgs.busybox
  ];

  extraCommands = ''
    mkdir tmp
  '';
}
