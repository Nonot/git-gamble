{
  pkgs,
}:

pkgs.dockerTools.buildLayeredImage {
  name = "container-image-docs";
  contents =
    [
      pkgs.dockerTools.binSh
      pkgs.busybox
    ]
    ++ import ../../shells/docs/packages.nix {
      inherit pkgs;
    };
}
