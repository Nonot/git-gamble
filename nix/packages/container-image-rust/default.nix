{
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-minimal pkgs;
in
pkgs.dockerTools.buildLayeredImage {
  name = "container-image-rust";
  contents = [
    pkgs.dockerTools.binSh
    pkgs.dockerTools.caCertificates
    rust-toolchain
    pkgs.stdenv.cc
  ];

  extraCommands = ''
    mkdir tmp
  '';
}
