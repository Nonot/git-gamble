{
  pkgs,
}:

pkgs.dockerTools.buildLayeredImage {
  name = "container-image-shell-test";
  contents =
    [
      pkgs.dockerTools.binSh
      pkgs.dockerTools.usrBinEnv
    ]
    ++ import ../../shells/shell/packages.nix {
      inherit pkgs;
    };

  extraCommands = ''
    mkdir tmp
  '';
}
