{
  inputs,
  pkgs,
}:

pkgs.dockerTools.buildLayeredImage {
  name = "container-image-rust-test";
  fromImage = inputs.self.packages."${pkgs.system}".container-image-rust;
  contents = [
    pkgs.dockerTools.usrBinEnv
    pkgs.gitMinimal
    pkgs.busybox
  ];

  enableFakechroot = true;
  fakeRootCommands = ''
    ${pkgs.dockerTools.shadowSetup}
    useradd normal_user
  '';
}
