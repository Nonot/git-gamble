{
  inputs,
  pkgs,
}:

pkgs.dockerTools.buildLayeredImage {
  name = "container-image-rust-test-coverage";
  fromImage = inputs.self.packages."${pkgs.system}".container-image-rust-test;
  contents = [
    pkgs.cargo-tarpaulin
  ];
}
