{
  inputs,
  lib,
  pkgs,
}:

let
  rust-toolchain = lib.internal.rust-toolchain-minimal pkgs;

  craneLib = (inputs.crane.mkLib pkgs).overrideToolchain rust-toolchain;
in
craneLib.buildDepsOnly {
  src = lib.snowfall.fs.get-file "";
  cargoLock = lib.snowfall.fs.get-file "Cargo.lock";
}
