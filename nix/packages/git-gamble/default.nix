{
  inputs,
  lib,
  pkgs,
}:

let
  root = lib.snowfall.fs.get-file "";

  rust-toolchain = lib.internal.rust-toolchain-minimal pkgs;

  craneLib = (inputs.crane.mkLib pkgs).overrideToolchain rust-toolchain;
in
craneLib.buildPackage {
  src = pkgs.lib.sources.cleanSource root;
  cargoArtifacts = inputs.self.packages."${pkgs.system}".git-gamble-binary;

  preCheck = ''
    patchShebangs tests/editor/fake_editor.sh
  '';

  buildInputs = [
    pkgs.gitMinimal
  ];

  nativeBuildInputs = [
    pkgs.makeWrapper
    pkgs.installShellFiles
  ];
  postInstall = ''
    wrapProgram $out/bin/git-gamble \
      --prefix PATH : "${pkgs.lib.makeBinPath [ pkgs.gitMinimal ]}"

    wrapProgram $out/bin/git-time-keeper \
      --prefix PATH : "${pkgs.lib.makeBinPath [ pkgs.gitMinimal ]}"

    export PATH="$PATH:$out/bin/"

    sh ./script/generate_completion.sh target/release/shell_completions/
    installShellCompletion --bash target/release/shell_completions/git-gamble.bash
    installShellCompletion --fish target/release/shell_completions/git-gamble.fish
    installShellCompletion --zsh target/release/shell_completions/_git-gamble

    sh ./script/usage.sh > git-gamble.1
    installManPage git-gamble.1
  '';
}
