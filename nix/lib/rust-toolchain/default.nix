{
  lib,
}:

let
  rustupToolchainFile = lib.snowfall.fs.get-file "rust-toolchain.toml";
  rustupToolchainFileContent = lib.trivial.importTOML rustupToolchainFile;
  rustupToolchain = rustupToolchainFileContent.toolchain;

  rust-toolchain-with =
    { pkgs, toolchainOverride }:
    let
      rustupToolchainOverriden = lib.snowfall.attrs.merge-deep [
        rustupToolchain
        toolchainOverride
      ];
    in
    pkgs.rust-bin.fromRustupToolchain rustupToolchainOverriden;
in
{
  inherit rust-toolchain-with;

  rust-toolchain-minimal =
    pkgs:
    rust-toolchain-with {
      inherit pkgs;
      toolchainOverride = {
        components = [ ];
        profile = "minimal";
      };
    };

  rust-toolchain-unchanged =
    pkgs:
    rust-toolchain-with {
      inherit pkgs;
      toolchainOverride = { };
    };
}
