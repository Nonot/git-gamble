use std::fs;
use std::io;
use std::path::PathBuf;

pub trait FolderIsNotEmptyExt {
	fn folder_is_not_empty(&self) -> Result<bool, io::Error>;
}

impl FolderIsNotEmptyExt for PathBuf {
	fn folder_is_not_empty(&self) -> Result<bool, io::Error> {
		let read_dir = fs::read_dir(self)?;
		Ok(read_dir.count() >= 1)
	}
}
